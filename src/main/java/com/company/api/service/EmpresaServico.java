package com.company.api.service;

import com.company.api.entity.Empresa;
import com.company.api.repository.EmpresaRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EmpresaServico {

    @Autowired
    EmpresaRepositorio repository;


    public List<Empresa> getAllEmpresa(Integer pageNo, Integer pageSize, String sortBy)
    {
        PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<Empresa> pagedResult = repository.findAll(paging);

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<Empresa>();
        }
    }

    public Empresa save(Empresa empresa){
        repository.save(empresa);
        return empresa;
    }
    public void delete(Long id){
        repository.deleteById(id);
    }

    public Optional<Empresa> findOne(Long id){
        return repository.findById(id);
    }

    public List<Empresa> findEmpresaByCnpj(String cnpj){
        return repository.findEmpresaByCnpj(cnpj);
    }
    public List<Empresa> findEmpresaByNome(String nome){
        return repository.findEmpresaByNome(nome);
    }
    public List<Empresa> findEmpresaByCnpjAndNome(String cnpj, String nome){
        return repository.findEmpresaByCnpjAndNome(cnpj, nome);
    }

}
