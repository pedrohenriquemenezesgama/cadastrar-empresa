package com.company.api.repository;

import com.company.api.entity.Empresa;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmpresaRepositorio extends PagingAndSortingRepository<Empresa, Long> {


    List<Empresa> findEmpresaByNome(String nome);

    List<Empresa> findEmpresaByCnpj(String cnpj);

    List<Empresa> findEmpresaByCnpjAndNome(String cnpj,String nome);
}


