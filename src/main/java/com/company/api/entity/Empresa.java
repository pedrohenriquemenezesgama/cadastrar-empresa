package com.company.api.entity;

import com.company.enums.TipoEmpresa;
import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@Table
public class Empresa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String nome;

    @NotNull
    private String cnpj;

    @NotNull
    private String contato;

    @NotNull
    private String cep;

    @NotNull
    private String bairro;

    @NotNull
    private String logradouro;

    @NotNull
    @Column(name = "tipo_empresa")
    private TipoEmpresa tipoEmpresa;

    @NotNull
    @Column(name = "razao_social")
    private String razaoSocial;

    @NotNull
    private String email;

    @NotNull
    private String estado;

    @NotNull
    private String cidade;

    private String complemento;

}
