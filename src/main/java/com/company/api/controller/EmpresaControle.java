package com.company.api.controller;

import com.company.api.entity.Empresa;
import com.company.api.service.EmpresaServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/empresas")
@CrossOrigin
public class EmpresaControle {

    @Autowired
    EmpresaServico service;

    @GetMapping
    public ResponseEntity<List<Empresa>> getAllEmpresa(
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "id") String sortBy)
    {
        List<Empresa> list = service.getAllEmpresa(pageNo, pageSize, sortBy);

        return new ResponseEntity<List<Empresa>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping
    Empresa create(@RequestBody Empresa empresa){
        return service.save(empresa);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable(name = "id") Long id){
        service.delete(id);
    }

    @PutMapping
    public Empresa update(@RequestBody Empresa empresa){
        return  service.save(empresa);
    }

    @GetMapping("/buscar/cnpj")
    public ResponseEntity<List<Empresa>> findEmpresaByCnpj(@RequestParam(name = "value") String cnpj){
        List<Empresa> list = service.findEmpresaByCnpj(cnpj);
        return new ResponseEntity<List<Empresa>>(list, new HttpHeaders(), HttpStatus.OK);
    }
    @GetMapping("/buscar/nome")
    public ResponseEntity<List<Empresa>> findEmpresaByNome(@RequestParam(name = "value") String nome){
        List<Empresa> list = service.findEmpresaByNome(nome);
        return new ResponseEntity<List<Empresa>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/buscar/nome/cnpj")
    public ResponseEntity<List<Empresa>> findEmpresaByCnpjAndNome(@RequestParam(name = "nome") String nome,@RequestParam(name = "cnpj") String cnpj){
        List<Empresa> list = service.findEmpresaByCnpjAndNome(cnpj,nome);
        return new ResponseEntity<List<Empresa>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/buscar/{id}")
    public Optional<Empresa> findById(@PathVariable(name = "id") Long id){
        return  service.findOne(id);
    }



}
